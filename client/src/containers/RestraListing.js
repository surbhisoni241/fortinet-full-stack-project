import React from 'react';
import  RestraListingComponent  from '../components/RestraListing';
import Filter from '../components/Filter';
import axios from 'axios';
import InfiniteScroll from 'react-infinite-scroller';

const FETCH_RES_API = '/restaurant?';
class RestraListing extends React.Component {
    constructor(){
        super()
        this.state ={
            restaurants: [],
            count: '',
            isFetchInProgress: false,
            page: 1,
            filters: {
              sortBy: ""
            }
        }
        this.onFilterUpdate = this.onFilterUpdate.bind(this);
        this.loadMore = this.loadMore.bind(this);
    }
    fetchRestaurantList(uri, append){
        axios.get(uri)
           .then((res) => {
             if (append) {
              let restaurants = this.state.restaurants.concat(res.data.restaurants);
                this.setState(()=> ({restaurants: restaurants, isFetchInProgress: false}));
             } else {
                this.setState(() => ({restaurants: res.data.restaurants, count: res.data.count, isFetchInProgress: false}))
             }
           })
           .catch(function(err){
             console.error(err)
           })
    }
    componentDidMount() {
      this.fetchRestaurantList(FETCH_RES_API);
    }
    _getUpdatedURL(filters, page) {
      let url = FETCH_RES_API;
      if (filters.sortBy) {
        url = url + 'sortBy=' + filters.sortBy + '&'
      }
      if(filters.cuisines){
        url = url + "cuisines=" + filters.cuisines.toLowerCase() + '&';
      }
      url += 'page=' + page;
      return url;
    }

    onFilterUpdate(filters) {
      this.setState(() => ({filters: filters}));
      let uri =  this._getUpdatedURL(filters, this.state.page);
      this.fetchRestaurantList(uri);
    }

    loadMore(page){
      console.log('next page is', page);
      let isFetchInProgress = this.state.isFetchInProgress;
      if (!isFetchInProgress) {
        this.setState(()=> ({page: page, isFetchInProgress: true}));
        let uri =  this._getUpdatedURL(this.state.filters, page);
        this.fetchRestaurantList(uri, true);
      }
    }
    render(){

        return (<div>
                <h3 className="restra-heading">Listing Restaurants ({this.state.count}) </h3>
                <Filter onFilterUpdate={this.onFilterUpdate}/>
                <InfiniteScroll
                   pageStart={1}
                   loadMore={this.loadMore}
                   hasMore={this.state.page*12 < this.state.count}
                   loader={<div className="loader" key={0}>Loading ...</div>}>
                <RestraListingComponent count={this.state.count} restaurants={this.state.restaurants}/>
                </InfiniteScroll>
          </div>);
    }
}

export default RestraListing;