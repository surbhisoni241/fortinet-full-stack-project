import React, { Component } from 'react';
import  Header  from './components/Header';
import Jumbotron  from './components/Jumbotron';
import  RestraListing  from './containers/RestraListing.js';
import  Footer from './components/Footer.js';

class App extends Component {
  constructor(){
    super();
    this.state = {}

  }
  render() {
    return (
      <div>
        <Header />
        <Jumbotron />
        <RestraListing />
        <Footer />
      </div>
    );
  }
}

export default App;
