import React from 'react';
import RestaurantCard from './RestaurantCard';
import Filter from './Filter';

class RestraListingComponent extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div>
                <div className="container">
                    <div className="row" style={{display: 'flex',listStyle: 'none'}}>
                       {this.props.restaurants.map((restaurant) => (
                        <RestaurantCard count={this.props.count} restaurant={restaurant} key={restaurant._id}/>
                        ))}  
                    </div>
                </div>
            </div>
        )
    }
}

export default RestraListingComponent;