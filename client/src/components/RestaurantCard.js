import React from 'react'

class RestaurantCard extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        const restaurant = this.props.restaurant;
        return(
            <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <div className="list-item" style={{backgroundColor:'#ffffff'}}>
                        <div className="list-item-inner">
                            <div className="list-imagecontainer list-subitem">
                              <span>
                                 <img src="/assets/css/Restaurant.jpg" alt="restra pic" />
                              </span>
                            </div>    
                            <div className="ribbon-wrapper-featured">
                              <div className="ribbon-featured">
                                  <i className="fa fa-star"></i> {restaurant.rating.aggregate_rating}<br/> 
                                  <span className="votes"><b>{restaurant.votes} votes</b></span>
                               </div>
                            </div>
                            <div className="listing-itemband">
                                  <div className="list-pricecontainer">
                                      <div className="listingitem-subelement res-price">
                                          <i className="fa fa-cutlery"></i> {restaurant.cuisinesList[0]}
                                      </div>
                                      <div className="listingitem-subelement res-price">
                                          <span className="-price-suffix">
                                            <i className="fa fa-circle" style={{color: 'green'}}></i>
                                           table booking </span>
                                      </div>
                                  </div>
                            </div>
                            <div className="listing-item-wrapper">
                                <div className="list-detailcontainer list-subitem">
                                    <div className="list-itemdetails">
                                        <span className="list-itemtitle">
                                            {restaurant.name}<br/>
                                        </span>
                                        <span className="list-address">
                                            <i className="fa fa-map-marker" aria-hidden="true"></i>{restaurant.location.address}
                                        </span><br/>
                                        <span className="list-address">
                                            <i className="fa fa-money"></i> Costs {restaurant.average_cost_for_two} for two
                                        </span>
                                    </div>
                                </div>
                                <div className="list-subdetailcontainer list-subitem">
                                    <div className="list-customfields">
                                        <div className="listingitem-subelement res-onlyitem">
                                            <span className="res-title">Accepts cash & online payments</span>
                                            <span className="res-rating-text"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
            </div>   
        )
    }
}

export default RestaurantCard;