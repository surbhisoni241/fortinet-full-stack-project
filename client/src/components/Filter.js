import React from 'react';

class Filter extends React.Component{

    constructor(){
        super();
        this.state ={
            sortBy: "", 
            cuisines: ""
        }
        this.handleSort = this.handleSort.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
    }

    handleSort(e){
       const sortBy = e.target.value;
       var oldState = this.state;
       oldState.sortBy = sortBy;
       this.setState(() => (oldState));

       this.props.onFilterUpdate(oldState);
    }

    handleFilter(e){
       const cuisines = e.target.value;
       var oldState = this.state;
       oldState.cuisines = cuisines;
       this.setState(() => (oldState));

       this.props.onFilterUpdate(oldState);
    }

    render(){
        return(
                <div className="container filter">
                     <div class="row">
                        <select class="form-control col-2 pull-right filter-item-filter" onChange={this.handleSort}>
                          <option  value="" default>Sort By</option>
                          <option value="rating.aggregate_rating">Rating</option>
                          <option value="votes">Votes</option>
                          <option value="average_cost_for_two">Price</option>
                        </select>

                         <select class="form-control col-2 filter-item-filter" onChange={this.handleFilter}>
                          <option  value="" default>Cousines</option>
                          <option>French</option>
                          <option>Japanese</option>
                          <option>Seafood</option>
                          <option>Chinese</option>
                          <option>Asian</option>
                        </select>
                      </div>
                      <br/>
                </div>
         )
    }
}

export default Filter;