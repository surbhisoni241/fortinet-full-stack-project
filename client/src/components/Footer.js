import React from 'react';

function Footer(props) {
    return(
        <footer className="footer">
           
            <div className="container">
              <p className="float-right">
                <a href="#">Back to top</a>
              </p>
            </div>
        </footer>
    )
}

export default Footer;


