const csv = require('csv');
const { mongoose } = require('./config/database');
const { Restaurant } = require('./app/models/Restaurant.js');
var obj = csv();
obj.from.path('../restaurantsa9.csv').to.array(function(data){

  data.forEach(function(row){ 
   
    var restaurantsObject = {
              hotel_id: row[0],
              name: row[1],
              cuisines: row[2],
              average_cost_for_two: row[3],
              currency: row[4],
              table_booking: row[5],
              rating:{
                  aggregate_rating: row[7],
                  rating_color: row[8],
                  rating_text: row[9],
              },
              votes: row[10],
              location:{
                country_code: row[11],
                city: row[12],
                address: row[13],
                locality: row[14],
                locality_verbose: row[15],
                longitude: row[16],
                latitude: row[17]
              }

    }
    console.log(restaurantsObject);
     restaurant = new Restaurant(restaurantsObject);
      restaurant.save()
        .then(function(res){
            console.log("One Hotel inserted in db", res)
        })
        .catch(function(err){
            console.log("error", err);
        })

   }) //forEach loop ended ...*/

})