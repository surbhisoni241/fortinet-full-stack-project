const mongoose = require("mongoose");


mongoose.promise = global.Promise;
let DB_URL = process.env.DB_URL || "mongodb://localhost:27017/restaurants-mngr";

mongoose.connect(DB_URL, {useNewUrlParser: true})
        .then(function(){
            console.log("Connected to DB");
        })
        .catch(function(){
            console.log("oops!! not connected to data");
        })

 module.exports = {
    mongoose
 }