const express = require('express');
const port = '3001';
const { mongoose } = require('./config/database.js');
const { restaurantsRouter } = require('./app/controllers/restaurantsControllers.js');
const { userRouter } = require('./app/controllers/userController.js');
const fs = require('fs')
const morgan = require('morgan')
const path = require('path')
const app = express();
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'});

app.use(express.json());

// server side code
app.use(express.static('../client/build'));

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('', function(req,res){
    res.send("app is working");
});

app.use('/', restaurantsRouter);
app.use('/', userRouter);
app.use(morgan('combined', {stream: accessLogStream})) //morgan middleware....
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    res.status(err.status || 500).send("The resource you are looking for doesn’t exist.");
    next(err);
});

app.listen(port, function(){
    console.log("restaurants App Listening on port: ", port)
    console.log("Morgan logger with express...")
});