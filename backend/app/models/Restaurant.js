const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const restaurantSchema = new Schema ({
 hotel_id: {
   type: String,
   required: true
 },
 name: {
    type: String,
    required: true
 },
 cuisines: {
    type: String,
    required: true
 },
  cuisinesList: {
    type: [String],
    required: true
 },
 average_cost_for_two: {
   type: Number,
   required: true
 },
 currency: {
    type: String,
   required: true
 },
 table_booking: {
   type: String,
   required: true
 },
 rating: {
    aggregate_rating: {
       type: Number,
       required: true
    },
    rating_color: {
        type: String,
        required: true
    },
    rating_text: {
       type: String,
       required: true
    }
 },
 votes: {
    type: Number,
    required: true
 },
 location: {
    country_code: {
        type: Number,
        required: true
    },
    city:{
       type: String ,
       required: true
    },
    address: {
       type: String ,
       required: true
    },
    locality: {
       type: String ,
       required: true
    },
    locality_verbose: {
       type: String ,
       required: true
    },
    longitude: {
       type: String ,
       required: true
    },
    latitude: {
       type: String ,
       required: true
    }
 }
})

const Restaurant = mongoose.model('Restaurant', restaurantSchema);

module.exports = {
    Restaurant
}