const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const validator = require('validator');
const bcryptjs = require('bcryptjs');

const userSchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        require: true,
        unique: true,
        validate : {
            validator: function(value){
                return validator.isEmail(value);
            },
            message: function(){
                return "invalid email format";
            }
        }
    },
    password:{
        type: String,
        required: true,
        required: true,
        minlength: 6,
        maxlength: 128
    },
})

userSchema.pre('save', function(next){
    const user = this;
    if(user.isNew){
        bcryptjs.genSalt(10)
                .then(function(salt){
                    bcryptjs.hash(user.password, salt)
                            .then(function(encryptedPass){
                                user.password = encryptedPass;
                                next()
                            })
                })
    } else {
        next()
    }
})

const User = mongoose.model('User', userSchema);

module.exports = {
    User
}