const express = require('express');
const router = express.Router();
const { User } = require('../models/User.js')

router.post('/user', function(req, res){
    const body = req.body;
    const user = new User(body);
    console.log("user for post");
    user.save()
        .then(function(response){
            console.log(response);
            res.send(response);
        })
        .catch(function(err){
            res.send(err);
        })
})

router.get('/user', function(req, res){
   User.find()
       .then(function(response){
          res.send(response);
       })
       .catch(function(err){
          res.send(err);
       })
})

router.get('/login', function(req, res){
    
})
module.exports = {
    userRouter: router
}