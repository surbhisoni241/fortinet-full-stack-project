const express = require('express');
const router = express.Router();
const { Restaurant } = require('../models/Restaurant.js');
const PAGE_SIZE = 12;
    const rating = []
    

router.get('/restaurant', function(req, res){

   const page = req.query.page || 1;
   const skip = (page -1) * 10;
   const limit = PAGE_SIZE;
   const sortBy = req.query.sortBy;
   let sort = {};
   let filter = {};
   
   if (req.query.cuisines){
    filter['cuisinesList'] = {$in:  [req.query.cuisines]}
   }
   sort[sortBy] = -1;

   var countPromise = Restaurant.count(filter);
   var queryPromise = Restaurant.find(filter)
        .sort(sort)
        .limit(limit)
        .skip(skip)
       Promise.all([countPromise, queryPromise]).then(function(restaurants){
            res.send({count: restaurants[0] , restaurants: restaurants[1]});
        })
        .catch(function(err){
            res.send(err);
        })
})
module.exports = {  
    restaurantsRouter: router
}